const { retrieveDocument, setupSpectral, resultsForCode } = require('@jamietanna/spectral-test-harness')

describe('semver', () => {
  test('fails when not a number', async () => {
    const spectral = await setupSpectral('ruleset.yaml')
    const document = retrieveDocument('semver/invalid.yaml')

    const results = resultsForCode(await spectral.run(document), 'semver')

    expect(results.length).toBeGreaterThanOrEqual(1)
  })

  test('passes when valid format', async () => {
    const spectral = await setupSpectral('ruleset.yaml')
    const document = retrieveDocument('semver/valid.yaml')

    const results = resultsForCode(await spectral.run(document), 'semver')

    expect(results).toHaveLength(0)
  })
})
