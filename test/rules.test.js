const { retrieveDocument, setupSpectral, getErrors } = require('@jamietanna/spectral-test-harness')

test('Complete document passes', async () => {
  const spectral = await setupSpectral('ruleset.yaml')
  const document = retrieveDocument('complete/valid.yaml')

  const results = getErrors(await spectral.run(document))

  expect(results).toHaveLength(0)
})
